"packadd! dracula
"syntax enable
syntax on
colorscheme night-owl 
highlight LineNr term=bold cterm=NONE ctermfg=white ctermbg=darkgrey gui=NONE guifg=DarkGrey guibg=NONE
highlight Normal ctermfg=white
set clipboard=unnamedplus
set title
set number relativenumber
set encoding=utf-8
set fileencodings=utf-8
set noerrorbells
set smartindent
set nowrap
set smartcase
set noswapfile
set nobackup                                                                                                                                                        
set incsearch
set expandtab
set cursorcolumn
set cursorline
"set statusline+=%L\ \ \ %f\ \ \ %l,%c\ \ \ %F
set laststatus=2
set hlsearch
"""""""""""""""""""""""""""""""""""""""""
autocmd VimEnter * ColorHighlight .
